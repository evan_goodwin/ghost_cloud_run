Run a Ghost Blog on Google's Cloud Run - a fully managed serverless platform. You will need a separate mysql database. 

More about Ghost
https://ghost.org/

More about Cloud Run
https://cloud.google.com/run

See set up instructions:
https://medium.com/p/be1a965f0674
